import React, { Component } from "react";
import {
  //UTF16toBinary,
  BinarytoUTF16,
  //reedMullerDecode,
} from "../reedMuller/ReedMuller";
import "./messageInfoStyles.css";

class MessageInfo extends Component {
  formatInBinary = (bits) => {
    let stringBuilder = "";
    if (bits == null) return null;
    for (let i = 0; i < bits.length; i += 8) {
      stringBuilder += bits.substring(i, i + 8) + "\n";
    }
    return stringBuilder;
  };

  formatBeforeEncoding = (bits) => {
    let stringBuilder = "";
    if (bits == null) return null;
    for (let i = 0; i < bits.length; i += 6) {
      let datagram = bits.substring(i, i + 6);
      if (datagram.length < 6) datagram = datagram.padStart(6, 0);
      stringBuilder += datagram + "\n";
    }
    return stringBuilder;
  };

  formatEncoded = (bits) => {
    let stringBuilder = "";
    if (bits == null) return null;
    for (let i = 0; i < bits.length; i += 32) {
      stringBuilder += bits.substring(i, i + 32) + "\n";
    }
    return stringBuilder;
  };

  formatReceived = (bits) => {
    let stringBuilder = "";
    if (bits == null) return null;
    for (let i = 0; i < bits.length; i += 32) {
      stringBuilder += bits.substring(i, i + 32) + "\n";
    }
    return stringBuilder;
  };

  formatErrorInformation = (rec, dec, d) => {
    let stringBuilder = "";
    for (let i = 0; i < rec.length; i += 32) {
      let recDatagram = Array.from(rec.substring(i, i + 32));
      let decDatagram = Array.from(dec.substring(i, i + 32));
      let xor = [];
      for (let j = 0; j < 32; j++) {
        xor.push(recDatagram[j] ^ decDatagram[j]);
      }
      let errorIndexArray = [];
      for (let j = 0; j < xor.length; j++) {
        if (xor[j] === 1) errorIndexArray.push(j + 1);
      }
      if (errorIndexArray.length === 0) {
        stringBuilder += "No errors for this datagram\n";
      } else if (errorIndexArray.length === 1) {
        // prettier-ignore
        stringBuilder += "Corrected an error at index: " + errorIndexArray[0] +"\n";
      } else if (errorIndexArray.length > 7) {
        // prettier-ignore
        stringBuilder += "Too many errors - Unable to correct\n";
      } else {
        // prettier-ignore
        stringBuilder += "Corrected " + errorIndexArray.length + " errors at indices: " 
                                      + errorIndexArray.join(", ") + "\n";
      }
    }
    // stringBuilder += "\n";
    // for (let i = 0; i < d.length; i += 8) {
    //   stringBuilder += d.substring(i, i + 8) + "\n";
    // }
    return stringBuilder;
  };

  render() {
    const { data, sender, user, bits } = this.props;
    console.log("bits =", bits, "data =", data);
    return (
      <React.Fragment>
        <div className="inspect-header">
          {user === sender ? (
            <div>Inspecting your message</div>
          ) : sender === null ? (
            <div>Select a message to inspect</div>
          ) : (
            <div>Inspecting {this.displayName(sender)}'s message</div>
          )}
        </div>

        {user === sender ? (
          // if user sent the message
          <React.Fragment>
            <div id="inspect-top-container-me">
              <div>
                <label>Message Content</label>
              </div>
              <div className="message-content-container user-sent">
                <p className="message-content">
                  {BinarytoUTF16(bits.bitsData)}
                </p>
              </div>
            </div>
            <hr></hr>
            <div id="inspect-lower-container-me">
              <div id="in-binary">
                <div>
                  <label>In binary</label>
                </div>
                <div
                //className="message-content-container user-sent"
                >
                  <textarea
                    readOnly={true}
                    id="in-binary-text"
                    className="form-control message-content"
                    value={this.formatInBinary(bits.bitsData)}
                  ></textarea>
                </div>
              </div>
              <div id="before-encoding">
                <div>
                  <label>Datagrams before encoding</label>
                </div>
                <div
                //className="message-content-container user-sent"
                >
                  <textarea
                    readOnly={true}
                    id="before-encoding-text"
                    className="form-control message-content"
                    value={this.formatBeforeEncoding(bits.bitsData)}
                  ></textarea>
                </div>
              </div>
              <div id="encoded">
                <div>
                  <label>Encoded datagrams (Sent data)</label>
                </div>
                <div
                //className="message-content-container user-sent"
                >
                  <textarea
                    readOnly={true}
                    id="encoded-text"
                    className="form-control message-content"
                    value={this.formatEncoded(bits.bitsEncoded)}
                  ></textarea>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : sender ? (
          // if user received the message
          <React.Fragment>
            <div id="inspect-top-container-me">
              <div>
                <label>Message Content</label>
              </div>
              <div className="message-content-container user-sent">
                <p className="message-content">
                  {BinarytoUTF16(bits.bitsDecoded)}
                </p>
              </div>
            </div>
            <hr></hr>
            <div id="inspect-lower-container-me">
              <div id="received-bits">
                <div>
                  <label>Received bits</label>
                </div>
                <div
                //className="message-content-container user-sent"
                >
                  <textarea
                    readOnly={true}
                    id="received-bits-text"
                    className="form-control info-text-container message-content"
                    value={this.formatReceived(bits.bitsReceived)}
                  ></textarea>
                </div>
              </div>
              <div id="reconstructed-bits">
                <div>
                  <label>Reconstructed bits</label>
                </div>
                <div
                //className="message-content-container user-sent"
                >
                  <textarea
                    readOnly={true}
                    id="reconstructed-bits-text"
                    className="form-control info-text-container message-content"
                    // value={this.formatReceived(this.state.bitsDecoded[0])}
                    value={this.formatReceived(bits.bitsDecodedLong)}
                  ></textarea>
                </div>
              </div>
              <div id="error-information">
                <div>
                  <label>Error information</label>
                </div>
                <div
                //className="message-content-container user-sent"
                >
                  <textarea
                    readOnly={true}
                    id="error-information-text"
                    className="form-control info-text-container message-content"
                    //value="Corrected bits on indices: {list, of, indices}, total corrected:, % of errors"
                    value={this.formatErrorInformation(
                      bits.bitsReceived,
                      bits.bitsDecodedLong,
                      bits.bitsDecoded
                    )}
                  ></textarea>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : null}
      </React.Fragment>
    );
  }

  displayName = (userEmail) => {
    if (typeof userEmail.charAt(0) === "string") {
      return (
        userEmail.charAt(0).toUpperCase() + userEmail.substring(1).split("@")[0]
      );
    } else {
      return userEmail.split("@")[0];
    }
  };
}

export default MessageInfo;
