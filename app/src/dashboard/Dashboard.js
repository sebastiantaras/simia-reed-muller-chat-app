import React, { Component } from "react";
import Conversations from "../conversations/Conversations";
import "./dashboardStyles.css";
import Chat from "../chatWindow/Chat";
import MessageInfo from "../messageInformation/messageInfo";
import NewChatForm from "../chatWindow/newChatForm";
import {
  logOut,
  createMessage,
  createConversation,
  //getConversations,
} from "../firebase/FirebaseFunctions";
import {
  UTF16toBinary,
  reedMullerEncode,
  reedMullerDecodeLong,
  reedMullerDecode,
  simulateCommChannel,
} from "../reedMuller/ReedMuller";
const firebase = require("firebase");

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      selectedChat: null,
      chats: [],
      createChatFormVisible: false,
      selectedMessage: null,
      selectedSender: null,
      bits: {
        //if user is sender
        bitsData: null,
        bitsPreEncoding: null,
        bitsEncoded: null,
        //if user is reciever
        bitsReceived: null,
        bitsDecodedLong: null,
        bitsDecoded: null,
      },
    };
  }

  toLogin = () => {
    this.props.history.push("/login");
  };

  newChatButtonClicked = () => {
    console.log("New chat button clicked!");
    this.setState({
      selectedChat: null,
      createChatFormVisible: true,
      selectedMessage: null,
      selectedSender: null,
    });
  };

  selectChat = (chatIndex) => {
    this.setState({
      selectedChat: chatIndex,
      createChatFormVisible: false,
      selectedMessage: null,
      selectedSender: null,
    });
  };

  submitMessage = (message) => {
    const key = this.createKey(
      this.state.email,
      this.state.chats[this.state.selectedChat].participants.filter(
        (user) => user !== this.state.email
      )[0]
    );
    createMessage(key, message, this.state.email);
  };

  createKey = (thisParticipant, otherParticipant) =>
    [thisParticipant, otherParticipant].sort().join(";");

  componentDidMount = () => {
    // const { conversations, email } = getConversations(this.props.history);
    // this.setState({
    //   email: email,
    //   chats: conversations,
    // });
    firebase.auth().onAuthStateChanged(async (user) => {
      if (!user) this.props.history.push("/login");
      else {
        await firebase
          .firestore()
          .collection("conversations")
          .where("participants", "array-contains", user.email)
          .onSnapshot((result) => {
            const conversations = result.docs.map((doc) => doc.data());
            this.setState({
              email: user.email,
              chats: conversations,
            });
          });
      }
    });
  };

  newConversation = async (message, email1, email2) => {
    let data = simulateCommChannel(reedMullerEncode(UTF16toBinary(message)));
    createConversation(data, email1, email2);
    this.setState({
      createChatFormVisible: false,
    });
    console.log("created convo");
  };

  messageSelected = (message, data, sender) => {
    sender === this.state.email
      ? this.setState({
          selectedMessage: data,
          selectedSender: sender,
          bits: {
            bitsData: UTF16toBinary(message),
            bitsPreEncoding: UTF16toBinary(message),
            bitsEncoded: reedMullerEncode(UTF16toBinary(message)),
          },
        })
      : this.setState({
          selectedMessage: data,
          selectedSender: sender,
          bits: {
            bitsReceived: data,
            bitsDecodedLong: reedMullerDecodeLong(data),
            bitsDecoded: reedMullerDecode(data),
          },
        });
  };

  render() {
    return (
      <>
        <div className="jumbotron" id="landingTitle">
          <div>
            <h2 id="mainTitle">SIMIA</h2>
            <h6>Reed-Muller chat app</h6>
          </div>
          <div className="loginButton">
            <button
              type="button"
              className="btn btn-light landingButton"
              onClick={() => logOut()}
            >
              Log out
            </button>
          </div>
        </div>
        <div id="userInterface">
          <div id="leftUI">
            <div id="conversations">
              <Conversations
                history={this.props.history}
                newChatHandler={this.newChatButtonClicked}
                selectChatHandler={this.selectChat}
                chats={this.state.chats}
                userEmail={this.state.email}
                selectedChatIndex={this.state.selectedChat}
              />
            </div>
          </div>
          <div id="centerUI">
            <div id="chatDisplay">
              {this.state.createChatFormVisible ? (
                <NewChatForm createConversationHandler={this.newConversation} />
              ) : (
                <Chat
                  submitMessageHandler={this.submitMessage}
                  messageSelectedHandler={this.messageSelected}
                  user={this.state.email}
                  chat={this.state.chats[this.state.selectedChat]}
                />
              )}
            </div>
          </div>

          <div id="rightUI">
            <div id="message-info-main-container">
              <MessageInfo
                data={this.state.selectedMessage}
                sender={this.state.selectedSender}
                user={this.state.email}
                bits={this.state.bits}
              />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Dashboard;
