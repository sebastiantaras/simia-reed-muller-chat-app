const firebase = require("firebase");

export const logInUser = (email, password, actions, history) => {
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(
      () => {
        history.push("/dashboard");
      },
      (error) => {
        alert("Incorrect credentials");
        actions.setSubmitting(false);
        actions.resetForm();
      }
    );
};

export const createUser = (email, password, actions, history) => {
  firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(
      (authRes) => {
        const userObject = {
          email: authRes.user.email,
        };

        firebase
          .firestore()
          .collection("users")
          .doc(email)
          .set(userObject)
          .then(
            () => {
              history.push("/login");
            },
            (databaseError) => {
              alert("Failed to add user to database");
              actions.setSubmitting(false);
              actions.resetForm();
            }
          );
      },
      (authError) => {
        alert("Failed to add user to database");
        actions.setSubmitting(false);
        actions.resetForm();
      }
    );
};

export const getConversation = (email1, email2) => {
  const key = [email1, email2].sort().join(";");
  return firebase.firestore().collection("conversations").doc(key).get();
};

export const getUsers = () => {
  return firebase.firestore().collection("users").get();
};

export const getCurrentEmail = () => {
  return firebase.auth().currentUser.email;
};

export const logOut = () => {
  firebase.auth().signOut();
};

export const createMessage = (docKey, data, sender) => {
  firebase
    .firestore()
    .collection("conversations")
    .doc(docKey)
    .update({
      messages: firebase.firestore.FieldValue.arrayUnion({
        sender,
        data,
      }),
    });
};

export const createConversation = async (message, email1, email2) => {
  const key = [email1, email2].sort().join(";");
  await firebase
    .firestore()
    .collection("conversations")
    .doc(key)
    .set({
      messages: [
        {
          sender: email2,
          data: message,
        },
      ],
      participants: [email1, email2],
    });
};

// export const getConversations = async (history) => {
//   firebase.auth().onAuthStateChanged(async (user) => {
//     if (!user) history.push("/login");
//     else {
//       await firebase
//         .firestore()
//         .collection("conversations")
//         .where("participants", "array-contains", user.email)
//         .onSnapshot(async (result) => {
//           const email = user.email;
//           console.log(email);
//           const conversations = result.docs.map((doc) => doc.data());
//           console.log(conversations);
//           return [conversations, email];
//         });
//     }
//   });
// };
