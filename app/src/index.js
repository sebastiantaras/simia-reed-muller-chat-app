import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { Route, BrowserRouter as Router } from "react-router-dom";
import Login from "./login/Login";
import Register from "./register/Register";
import Dashboard from "./dashboard/Dashboard";
import Landing from "./Landing/Landing";
import "bootstrap/dist/css/bootstrap.min.css";
const firebase = require("firebase");
require("firebase/firestore");

firebase.initializeApp({
  apiKey: "AIzaSyB2-SezDKxNrQQZzi6OFG0_1Yw0AD-eSrE",
  authDomain: "zavrsni-86244.firebaseapp.com",
  databaseURL: "https://zavrsni-86244.firebaseio.com",
  projectId: "zavrsni-86244",
  storageBucket: "zavrsni-86244.appspot.com",
  messagingSenderId: "1045468764994",
  appId: "1:1045468764994:web:73a467cb2423cca5d606db",
  measurementId: "G-P9KTBP94M1",
});

const routing = (
  <Router>
    <div id="routing-container">
      <Route exact path="/" component={Landing}></Route>
      <Route exact path="/dashboard" component={Dashboard}></Route>
      <Route exact path="/login" component={Login}></Route>
      <Route exact path="/register" component={Register}></Route>
    </div>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
