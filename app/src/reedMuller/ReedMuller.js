// prettier-ignore
let v0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ]
// prettier-ignore
let v1 = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, ]
// prettier-ignore
let v2 = [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, ]
// prettier-ignore
let v3 = [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, ]
// prettier-ignore
let v4 = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, ]
// prettier-ignore
let v5 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ]

let generatorMatrix = [v0, v1, v2, v3, v4, v5];

let v = [v0, v1, v2, v3, v4, v5];

export const UTF16toBinary = (message) => {
  //console.log("message =", message);
  var stringBuilder = "";
  if (message == null) return null;
  for (var i = 0; i < message.length; i++) {
    stringBuilder += message[i].charCodeAt(0).toString(2).padStart(8, 0);
  }
  //console.log(stringBuilder);
  return stringBuilder;
};

export const BinarytoUTF16 = (message) => {
  var stringBuilder = "";
  if (message == null) return null;
  for (var i = 0; i < message.length; i += 8) {
    stringBuilder += String.fromCharCode(
      parseInt(message.substring(i, i + 8), 2)
    );
  }
  return stringBuilder;
};

export const multiplyVecMat = (datagram) => {
  let result = [];
  let vector = Array.from(datagram);
  for (let i = 0; i < 32; i++) {
    let sum = 0;
    for (let j = 0; j < 6; j++) {
      //console.log(vector[j], generatorMatrix[j][i]);
      sum += parseInt(vector[j]) * parseInt(generatorMatrix[j][i]);
    }
    //console.log(sum);
    if (sum % 2 === 0) result.push(0);
    else result.push(1);
  }
  return result;
};

export const reedMullerEncode = (message) => {
  // prettier-ignore
  //console.log("imat cemo ", Math.floor(message.length / 6), "datagrama\nostatak = ", message.length % 6);

  let stringBuilder = "";
  let encodedDatagram = "";
  for (let i = 0; i < message.length; i += 6) {
    let datagram = message.substring(i, i + 6);
    if (datagram.length < 6) datagram = datagram.padStart(6, 0);
    //console.log(datagram);

    //multiply datagram with genMatrix
    encodedDatagram = multiplyVecMat(datagram);
    stringBuilder += encodedDatagram.join("");
    //console.log(encodedDatagram);
  }
  //console.log("sent data\n", stringBuilder, stringBuilder.length);
  return stringBuilder;
};

export const reedMullerDecode = (data) => {
  //console.log("decoding\n", data);
  let stringBuilder = "";
  let result = "";
  if (data == null) return null;
  let trimAmount = (6 * (data.length / 32)) % 8;
  for (let i = 0; i < data.length; i += 32) {
    let datagram = data.substring(i, i + 32).split("");
    //console.log("datagram =", datagram.join(""));
    // definicija bitova a1-a5
    let a1 = [
      parseInt(datagram[0]) ^ parseInt(datagram[1]),
      parseInt(datagram[2]) ^ parseInt(datagram[3]),
      parseInt(datagram[4]) ^ parseInt(datagram[5]),
      parseInt(datagram[6]) ^ parseInt(datagram[7]),
      parseInt(datagram[8]) ^ parseInt(datagram[9]),
      parseInt(datagram[10]) ^ parseInt(datagram[11]),
      parseInt(datagram[12]) ^ parseInt(datagram[13]),
      parseInt(datagram[14]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[17]),
      parseInt(datagram[18]) ^ parseInt(datagram[19]),
      parseInt(datagram[20]) ^ parseInt(datagram[21]),
      parseInt(datagram[22]) ^ parseInt(datagram[23]),
      parseInt(datagram[24]) ^ parseInt(datagram[25]),
      parseInt(datagram[26]) ^ parseInt(datagram[27]),
      parseInt(datagram[28]) ^ parseInt(datagram[29]),
      parseInt(datagram[30]) ^ parseInt(datagram[31]),
    ];

    let a2 = [
      parseInt(datagram[0]) ^ parseInt(datagram[2]),
      parseInt(datagram[1]) ^ parseInt(datagram[3]),
      parseInt(datagram[4]) ^ parseInt(datagram[6]),
      parseInt(datagram[5]) ^ parseInt(datagram[7]),
      parseInt(datagram[8]) ^ parseInt(datagram[10]),
      parseInt(datagram[9]) ^ parseInt(datagram[11]),
      parseInt(datagram[12]) ^ parseInt(datagram[14]),
      parseInt(datagram[13]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[18]),
      parseInt(datagram[17]) ^ parseInt(datagram[19]),
      parseInt(datagram[20]) ^ parseInt(datagram[22]),
      parseInt(datagram[21]) ^ parseInt(datagram[23]),
      parseInt(datagram[24]) ^ parseInt(datagram[26]),
      parseInt(datagram[25]) ^ parseInt(datagram[27]),
      parseInt(datagram[28]) ^ parseInt(datagram[30]),
      parseInt(datagram[29]) ^ parseInt(datagram[31]),
    ];

    let a3 = [
      parseInt(datagram[0]) ^ parseInt(datagram[4]),
      parseInt(datagram[1]) ^ parseInt(datagram[5]),
      parseInt(datagram[2]) ^ parseInt(datagram[6]),
      parseInt(datagram[3]) ^ parseInt(datagram[7]),
      parseInt(datagram[8]) ^ parseInt(datagram[12]),
      parseInt(datagram[9]) ^ parseInt(datagram[13]),
      parseInt(datagram[10]) ^ parseInt(datagram[14]),
      parseInt(datagram[11]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[20]),
      parseInt(datagram[17]) ^ parseInt(datagram[21]),
      parseInt(datagram[18]) ^ parseInt(datagram[22]),
      parseInt(datagram[19]) ^ parseInt(datagram[23]),
      parseInt(datagram[24]) ^ parseInt(datagram[28]),
      parseInt(datagram[25]) ^ parseInt(datagram[29]),
      parseInt(datagram[26]) ^ parseInt(datagram[30]),
      parseInt(datagram[27]) ^ parseInt(datagram[31]),
    ];

    let a4 = [
      parseInt(datagram[0]) ^ parseInt(datagram[8]),
      parseInt(datagram[1]) ^ parseInt(datagram[9]),
      parseInt(datagram[2]) ^ parseInt(datagram[10]),
      parseInt(datagram[3]) ^ parseInt(datagram[11]),
      parseInt(datagram[4]) ^ parseInt(datagram[12]),
      parseInt(datagram[5]) ^ parseInt(datagram[13]),
      parseInt(datagram[6]) ^ parseInt(datagram[14]),
      parseInt(datagram[7]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[24]),
      parseInt(datagram[17]) ^ parseInt(datagram[25]),
      parseInt(datagram[18]) ^ parseInt(datagram[26]),
      parseInt(datagram[19]) ^ parseInt(datagram[27]),
      parseInt(datagram[20]) ^ parseInt(datagram[28]),
      parseInt(datagram[21]) ^ parseInt(datagram[29]),
      parseInt(datagram[22]) ^ parseInt(datagram[30]),
      parseInt(datagram[23]) ^ parseInt(datagram[31]),
    ];

    let a5 = [
      parseInt(datagram[0]) ^ parseInt(datagram[16]),
      parseInt(datagram[1]) ^ parseInt(datagram[17]),
      parseInt(datagram[2]) ^ parseInt(datagram[18]),
      parseInt(datagram[3]) ^ parseInt(datagram[19]),
      parseInt(datagram[4]) ^ parseInt(datagram[20]),
      parseInt(datagram[5]) ^ parseInt(datagram[21]),
      parseInt(datagram[6]) ^ parseInt(datagram[22]),
      parseInt(datagram[7]) ^ parseInt(datagram[23]),
      parseInt(datagram[8]) ^ parseInt(datagram[24]),
      parseInt(datagram[9]) ^ parseInt(datagram[25]),
      parseInt(datagram[10]) ^ parseInt(datagram[26]),
      parseInt(datagram[11]) ^ parseInt(datagram[27]),
      parseInt(datagram[12]) ^ parseInt(datagram[28]),
      parseInt(datagram[13]) ^ parseInt(datagram[29]),
      parseInt(datagram[14]) ^ parseInt(datagram[30]),
      parseInt(datagram[15]) ^ parseInt(datagram[31]),
    ];

    let aArray = [a1, a2, a3, a4, a5];
    let a = [];
    for (let i = 0; i < aArray.length; i++) {
      let countOnes = 0;
      let countZeros = 0;
      for (let j = 0; j < aArray[i].length; j++) {
        if (aArray[i][j] === 0) countZeros++;
        else countOnes++;
      }
      countOnes > countZeros ? a.push(1) : a.push(0);
    }

    let a0 = datagram;
    for (let i = 0; i < a.length; i++) {
      // prettier-ignore
      if (a[i]) a0 = a0.map((e, j) => e ^ v[i+1][j])
    }
    //console.log("a0", a0);
    let countOnes = 0;
    let countZeros = 0;
    for (let i = 0; i < a0.length; i++) {
      if (parseInt(a0[i]) === 0) countZeros++;
      else countOnes++;
    }
    countOnes > countZeros ? (a0 = 1) : (a0 = 0);
    a.unshift(a0);
    //console.log("a = ", a);

    //reconstruct datagram from errors
    let reconstructedData = new Array(32).fill(0);
    for (let i = 0; i < a.length; i++) {
      // prettier-ignore
      if (a[i]) reconstructedData = reconstructedData.map((e, j) => e ^ v[i][j]);
    }
    stringBuilder += reconstructedData.join("");

    // Trim last datagram condition
    if (i + 32 >= data.length) {
      let lastPart = a.join("");
      result += lastPart.substring(trimAmount, lastPart.length);
    } else result += a.join("");
  }
  //console.log("reconstructed data\n", stringBuilder, stringBuilder.length);
  //console.log("message =", result);
  // var temp = "";
  // for (var i = 0; i < result.length; i += 8) {
  //   temp += String.fromCharCode(parseInt(result.substring(i, i + 8), 2));
  // }
  //console.log(temp);
  return result;
};

export const reedMullerDecodeLong = (data) => {
  //console.log("decoding\n", data);
  let stringBuilder = "";
  let result = "";
  if (data == null) return null;
  let trimAmount = (6 * (data.length / 32)) % 8;
  for (let i = 0; i < data.length; i += 32) {
    let datagram = data.substring(i, i + 32).split("");
    //console.log("datagram =", datagram.join(""));
    // definicija bitova a1-a5
    let a1 = [
      parseInt(datagram[0]) ^ parseInt(datagram[1]),
      parseInt(datagram[2]) ^ parseInt(datagram[3]),
      parseInt(datagram[4]) ^ parseInt(datagram[5]),
      parseInt(datagram[6]) ^ parseInt(datagram[7]),
      parseInt(datagram[8]) ^ parseInt(datagram[9]),
      parseInt(datagram[10]) ^ parseInt(datagram[11]),
      parseInt(datagram[12]) ^ parseInt(datagram[13]),
      parseInt(datagram[14]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[17]),
      parseInt(datagram[18]) ^ parseInt(datagram[19]),
      parseInt(datagram[20]) ^ parseInt(datagram[21]),
      parseInt(datagram[22]) ^ parseInt(datagram[23]),
      parseInt(datagram[24]) ^ parseInt(datagram[25]),
      parseInt(datagram[26]) ^ parseInt(datagram[27]),
      parseInt(datagram[28]) ^ parseInt(datagram[29]),
      parseInt(datagram[30]) ^ parseInt(datagram[31]),
    ];

    let a2 = [
      parseInt(datagram[0]) ^ parseInt(datagram[2]),
      parseInt(datagram[1]) ^ parseInt(datagram[3]),
      parseInt(datagram[4]) ^ parseInt(datagram[6]),
      parseInt(datagram[5]) ^ parseInt(datagram[7]),
      parseInt(datagram[8]) ^ parseInt(datagram[10]),
      parseInt(datagram[9]) ^ parseInt(datagram[11]),
      parseInt(datagram[12]) ^ parseInt(datagram[14]),
      parseInt(datagram[13]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[18]),
      parseInt(datagram[17]) ^ parseInt(datagram[19]),
      parseInt(datagram[20]) ^ parseInt(datagram[22]),
      parseInt(datagram[21]) ^ parseInt(datagram[23]),
      parseInt(datagram[24]) ^ parseInt(datagram[26]),
      parseInt(datagram[25]) ^ parseInt(datagram[27]),
      parseInt(datagram[28]) ^ parseInt(datagram[30]),
      parseInt(datagram[29]) ^ parseInt(datagram[31]),
    ];

    let a3 = [
      parseInt(datagram[0]) ^ parseInt(datagram[4]),
      parseInt(datagram[1]) ^ parseInt(datagram[5]),
      parseInt(datagram[2]) ^ parseInt(datagram[6]),
      parseInt(datagram[3]) ^ parseInt(datagram[7]),
      parseInt(datagram[8]) ^ parseInt(datagram[12]),
      parseInt(datagram[9]) ^ parseInt(datagram[13]),
      parseInt(datagram[10]) ^ parseInt(datagram[14]),
      parseInt(datagram[11]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[20]),
      parseInt(datagram[17]) ^ parseInt(datagram[21]),
      parseInt(datagram[18]) ^ parseInt(datagram[22]),
      parseInt(datagram[19]) ^ parseInt(datagram[23]),
      parseInt(datagram[24]) ^ parseInt(datagram[28]),
      parseInt(datagram[25]) ^ parseInt(datagram[29]),
      parseInt(datagram[26]) ^ parseInt(datagram[30]),
      parseInt(datagram[27]) ^ parseInt(datagram[31]),
    ];

    let a4 = [
      parseInt(datagram[0]) ^ parseInt(datagram[8]),
      parseInt(datagram[1]) ^ parseInt(datagram[9]),
      parseInt(datagram[2]) ^ parseInt(datagram[10]),
      parseInt(datagram[3]) ^ parseInt(datagram[11]),
      parseInt(datagram[4]) ^ parseInt(datagram[12]),
      parseInt(datagram[5]) ^ parseInt(datagram[13]),
      parseInt(datagram[6]) ^ parseInt(datagram[14]),
      parseInt(datagram[7]) ^ parseInt(datagram[15]),
      parseInt(datagram[16]) ^ parseInt(datagram[24]),
      parseInt(datagram[17]) ^ parseInt(datagram[25]),
      parseInt(datagram[18]) ^ parseInt(datagram[26]),
      parseInt(datagram[19]) ^ parseInt(datagram[27]),
      parseInt(datagram[20]) ^ parseInt(datagram[28]),
      parseInt(datagram[21]) ^ parseInt(datagram[29]),
      parseInt(datagram[22]) ^ parseInt(datagram[30]),
      parseInt(datagram[23]) ^ parseInt(datagram[31]),
    ];

    let a5 = [
      parseInt(datagram[0]) ^ parseInt(datagram[16]),
      parseInt(datagram[1]) ^ parseInt(datagram[17]),
      parseInt(datagram[2]) ^ parseInt(datagram[18]),
      parseInt(datagram[3]) ^ parseInt(datagram[19]),
      parseInt(datagram[4]) ^ parseInt(datagram[20]),
      parseInt(datagram[5]) ^ parseInt(datagram[21]),
      parseInt(datagram[6]) ^ parseInt(datagram[22]),
      parseInt(datagram[7]) ^ parseInt(datagram[23]),
      parseInt(datagram[8]) ^ parseInt(datagram[24]),
      parseInt(datagram[9]) ^ parseInt(datagram[25]),
      parseInt(datagram[10]) ^ parseInt(datagram[26]),
      parseInt(datagram[11]) ^ parseInt(datagram[27]),
      parseInt(datagram[12]) ^ parseInt(datagram[28]),
      parseInt(datagram[13]) ^ parseInt(datagram[29]),
      parseInt(datagram[14]) ^ parseInt(datagram[30]),
      parseInt(datagram[15]) ^ parseInt(datagram[31]),
    ];

    let aArray = [a1, a2, a3, a4, a5];
    let a = [];
    for (let i = 0; i < aArray.length; i++) {
      let countOnes = 0;
      let countZeros = 0;
      for (let j = 0; j < aArray[i].length; j++) {
        if (aArray[i][j] === 0) countZeros++;
        else countOnes++;
      }
      countOnes > countZeros ? a.push(1) : a.push(0);
    }

    let a0 = datagram;
    for (let i = 0; i < a.length; i++) {
      // prettier-ignore
      if (a[i]) a0 = a0.map((e, j) => e ^ v[i+1][j])
    }
    //console.log("a0", a0);
    let countOnes = 0;
    let countZeros = 0;
    for (let i = 0; i < a0.length; i++) {
      if (parseInt(a0[i]) === 0) countZeros++;
      else countOnes++;
    }
    countOnes > countZeros ? (a0 = 1) : (a0 = 0);
    a.unshift(a0);
    //console.log("a = ", a);

    //reconstruct datagram from errors
    let reconstructedData = new Array(32).fill(0);
    for (let i = 0; i < a.length; i++) {
      // prettier-ignore
      if (a[i]) reconstructedData = reconstructedData.map((e, j) => e ^ v[i][j]);
    }
    stringBuilder += reconstructedData.join("");

    // Trim last datagram condition
    if (i + 32 >= data.length) {
      let lastPart = a.join("");
      result += lastPart.substring(trimAmount, lastPart.length);
    } else result += a.join("");
  }
  //console.log("reconstructed data\n", stringBuilder, stringBuilder.length);
  //console.log("message =", result);
  // var temp = "";
  // for (var i = 0; i < result.length; i += 8) {
  //   temp += String.fromCharCode(parseInt(result.substring(i, i + 8), 2));
  // }
  //console.log(temp);
  return stringBuilder;
};

export const simulateCommChannel = (data, more = false) => {
  //console.log("simulating errors on errors on\n", data, data.length);
  let stringBuilder = "";
  let chance = 7 / 34;
  if (more) chance = Math.random();

  //console.log();
  for (let i = 0; i < data.length; i += 32) {
    let numOfErrors = 0;
    let datagram = Array.from(data.substring(i, i + 32));
    let datagramWithErrors = [];
    for (let j = 0; j < datagram.length; j++) {
      let random = Math.random();
      if (numOfErrors < 7 && chance > random) {
        datagramWithErrors.push(datagram[j] ^ 1);
        numOfErrors++;
      } else {
        datagramWithErrors.push(datagram[j]);
      }
    }
    stringBuilder += datagramWithErrors.join("");
    // prettier-ignore
    //console.log("usporedi\n", datagram.join(""), "\n", datagramWithErrors.join(""));
  }
  //console.log("Sb\n", stringBuilder, "\n", data);
  return stringBuilder;
};
