import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import "./newChatFormStyles.css";
import {
  getConversation,
  getUsers,
  getCurrentEmail,
} from "../firebase/FirebaseFunctions";

function NewChatForm(props) {
  const emailIsValid = async (email) => {
    const users = await getUsers();
    return users.docs.map((doc) => doc.data().email).includes(email);
  };

  const conversationAlreadyExists = async (email1, email2) => {
    const conversation = await getConversation(email1, email2);
    return conversation.exists;
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email("Not a valid email")
      .required("Email is a required field"),

    message: Yup.string().required("Please enter a message"),
  });

  const formikInstance = useFormik({
    initialValues: {
      email: "",
      message: "",
    },
    onSubmit: async (values, actions) => {
      actions.setSubmitting(true);
      console.log(values);
      if (values.email !== getCurrentEmail()) {
        const valid = await emailIsValid(values.email);
        if (valid) {
          const exists = await conversationAlreadyExists(
            values.email,
            getCurrentEmail()
          );
          if (!exists) {
            props.createConversationHandler(
              values.message,
              values.email,
              getCurrentEmail()
            );
            actions.setSubmitting(false);
            actions.resetForm();
            return;
          }
        }
      }
      alert(
        `Failed to create a new conversation with ${values.email}. \nA conversation with the user already exists or the user doesn't exist or you entered your own email`
      );
      actions.setSubmitting(false);
      actions.resetForm();
    },
    validationSchema,
  });

  return (
    <React.Fragment>
      <div id="new-chat-form-header">Start a new conversation</div>
      <div className="container w-75 p-3">
        <form onSubmit={formikInstance.handleSubmit}>
          <div className="form-group" id="new-chat-form">
            <label>
              <b>Reciever's email</b>
            </label>
            <input
              name="email"
              type="email"
              className="form-control"
              placeholder="Enter reciever's email"
              onChange={formikInstance.handleChange}
              value={formikInstance.values.email}
            ></input>
            {formikInstance.errors.email && formikInstance.touched.email ? (
              <p className="errorInfo">{formikInstance.errors.email}</p>
            ) : null}
          </div>
          <div className="form-group">
            <label>
              <b>Your message</b>
            </label>
            <textarea
              type="text"
              name="message"
              className="form-control"
              placeholder="Enter message"
              onChange={formikInstance.handleChange}
              value={formikInstance.values.message}
            ></textarea>
            {formikInstance.errors.message && formikInstance.touched.message ? (
              <p className="errorInfo">{formikInstance.errors.message}</p>
            ) : null}
          </div>
          <hr />
          <button
            type="submit"
            className="btn btn-primary"
            id="confirm-new-chat-message"
            disabled={formikInstance.isSubmitting}
          >
            Send message
          </button>
        </form>
      </div>
    </React.Fragment>
  );
}

export default NewChatForm;
