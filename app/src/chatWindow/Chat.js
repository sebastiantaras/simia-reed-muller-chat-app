import React, { Component } from "react";
import {
  reedMullerEncode,
  reedMullerDecode,
  BinarytoUTF16,
  UTF16toBinary,
  simulateCommChannel,
} from "../reedMuller/ReedMuller";
import "./chatStyles.css";

class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: "",
    };
  }

  displayName = (userEmail) => {
    if (typeof userEmail.charAt(0) === "string") {
      return (
        userEmail.charAt(0).toUpperCase() + userEmail.substring(1).split("@")[0]
      );
    } else {
      return userEmail.split("@")[0];
    }
  };

  updateMessageValue = (e) => {
    if (e.keyCode === 13) {
      if (this.state.message && this.state.message.trim()) {
        this.sendMessage();
      }
    } else {
      this.setState({
        message: e.target.value,
      });
      //console.log(e.target.value);
    }
  };

  sendMessage = () => {
    let encodedBitsWithErrors = simulateCommChannel(
      reedMullerEncode(UTF16toBinary(this.state.message.trim()))
    );
    //console.log(UTF16toBinary(this.state.message.trim()));
    document.getElementById("enter-message-field").value = "";
    if (this.state.message.length > 0)
      this.props.submitMessageHandler(encodedBitsWithErrors);
  };

  fromUTF16toBinary = (message) => {
    let stringBuilder = "";
    for (let i = 0; i < message.length; i++) {
      stringBuilder += message[i].charCodeAt(0).toString(2).padStart(8, 0);
    }
    //console.log(stringBuilder);
    reedMullerEncode(stringBuilder);
  };

  componentDidUpdate = () => {
    if (document.getElementById("messages"))
      document
        .getElementById("messages")
        .scrollTo(0, document.getElementById("messages").scrollHeight);
  };

  render() {
    const { chat, user } = this.props;
    if (chat === undefined || chat.messages < 1) {
      return (
        <React.Fragment>
          <div className="chatHeader">Select a chat or create a new one</div>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <div className="chatHeader">
            Talking to {chat.participants.filter((usr) => usr !== user)[0]}
          </div>
          <div>
            <hr id="hline-top-of-chat"></hr>
            <div id="messages">
              <div>
                {chat.messages.map((message, index) => {
                  return (
                    <React.Fragment key={index}>
                      <div key={index} className="user-messages-container">
                        <p className="message">
                          {user === message.sender
                            ? "Me"
                            : this.displayName(message.sender)}
                        </p>
                        <div
                          className={
                            message.sender === user
                              ? "message-container user-sent-message"
                              : "message-container received-message"
                          }
                          onClick={() =>
                            this.props.messageSelectedHandler(
                              BinarytoUTF16(reedMullerDecode(message.data)),
                              message.data,
                              message.sender
                            )
                          }
                        >
                          <p className="message">
                            {BinarytoUTF16(reedMullerDecode(message.data))}
                          </p>
                        </div>
                      </div>
                    </React.Fragment>
                  );
                })}
              </div>
            </div>
          </div>
          <React.Fragment>
            <hr className="hline-small-margin"></hr>
            {/*OVAJ DIO U ZASEBNU KOMPONENTU*/}
            <div id="enter-message-container">
              <textarea
                type="text"
                id="enter-message-field"
                className="form-control"
                htmlFor="enter-message"
                spellCheck="false"
                onKeyUp={(e) => this.updateMessageValue(e)}
                placeholder={
                  "Message " +
                  this.displayName(
                    chat.participants.filter((usr) => usr !== user)[0]
                  )
                }
              ></textarea>
              <div id="simulate-errors-container">
                <button
                  id="send-message-btn"
                  className="btn btn-dark"
                  onClick={this.sendMessage}
                >
                  Send
                </button>
              </div>
            </div>
          </React.Fragment>
        </React.Fragment>
      );
    }
  }
}

export default Chat;
