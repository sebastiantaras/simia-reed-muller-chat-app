import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import "./loginStyles.css";
import { logInUser } from "../firebase/FirebaseFunctions";

function Login() {
  let history = useHistory();

  const toLanding = () => {
    history.push("/");
  };

  const toRegister = () => {
    history.push("/register");
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email("Not a valid email")
      .required("Email is a required field"),

    password: Yup.string().required("Password is a required field"),
  });

  const formikInstance = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: (values, actions) => {
      actions.setSubmitting(true);
      logInUser(values.email, values.password, actions, history);
    },
    validationSchema,
  });

  return (
    <React.Fragment>
      <div className="jumbotron" id="landingTitle">
        <div>
          <h2 onClick={() => toLanding()} id="mainTitle">
            SIMIA
          </h2>
          <h6>Reed-Muller chat app</h6>
        </div>
      </div>
      <div className="container w-25 p-3" id="formContainer">
        <form onSubmit={formikInstance.handleSubmit}>
          <h1 className="loginHeader">LOGIN</h1>
          <hr />
          <div className="form-group">
            <label htmlFor="email">
              <b>Email</b>
            </label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              name="email"
              onChange={formikInstance.handleChange}
              value={formikInstance.values.email}
            ></input>
            {formikInstance.errors.email && formikInstance.touched.email ? (
              <p className="errorInfo">{formikInstance.errors.email}</p>
            ) : null}
          </div>
          <div className="form-group">
            <label htmlFor="password">
              <b>Password</b>
            </label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter password"
              name="password"
              onChange={formikInstance.handleChange}
              value={formikInstance.values.password}
            ></input>
            {formikInstance.errors.password &&
            formikInstance.touched.password ? (
              <p className="errorInfo">{formikInstance.errors.password}</p>
            ) : null}
          </div>
          <hr />
          <button
            type="submit"
            className="btn btn-primary"
            id="submitFormButton"
            disabled={formikInstance.isSubmitting}
          >
            Log in
          </button>
          <div id="question" onClick={() => toRegister()}>
            Dont have an account?
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}

export default Login;
