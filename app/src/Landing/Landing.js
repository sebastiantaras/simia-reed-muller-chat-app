import React, { Component } from "react";
import "./landingStyles.css";

class Landing extends Component {
  toLogin = () => {
    this.props.history.push("/login");
  };

  toLanding = () => {
    this.props.history.push("/");
  };

  render() {
    return (
      <>
        <div className="jumbotron" id="landingTitle">
          <div>
            <h2 onClick={() => this.toLanding()} id="mainTitle">
              SIMIA
            </h2>
            <h6>Reed-Muller chat app</h6>
          </div>
          <div className="loginButton">
            <button
              type="button"
              className="btn btn-light landingButton"
              onClick={() => this.toLogin()}
            >
              Proceed to login
            </button>
          </div>
        </div>
      </>
    );
  }
}

export default Landing;
