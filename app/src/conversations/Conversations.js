import React, { Component } from "react";
import "./conversationsStyles.css";

class Conversations extends Component {
  displayName = (chat, userEmail) => {
    return (
      chat.participants
        .filter((user) => user !== userEmail)[0]
        .split("@")[0]
        .charAt(0)
        .toUpperCase() +
      chat.participants
        .filter((user) => user !== userEmail)[0]
        .split("@")[0]
        .substring(1)
    );
  };

  selectChat = (index) => this.props.selectChatHandler(index);

  render() {
    const { chats, userEmail, selectedChatIndex } = this.props;
    if (chats.length > 0)
      return (
        <React.Fragment>
          <button
            variant="contained"
            className="btn btn-dark"
            id="newChatButton"
            onClick={this.props.newChatHandler}
          >
            Create new chat
          </button>
          <div id="conversationsList">
            <list>
              {chats.map((chat, index) => {
                return (
                  <React.Fragment key={index}>
                    {index !== 0 ? <hr className="divider"></hr> : null}

                    <ul
                      key={index}
                      onClick={() => {
                        this.selectChat(index);
                      }}
                      className={
                        selectedChatIndex === index
                          ? "unselectedChat selectedChat"
                          : "unselectedChat"
                      }
                    >
                      <b>{this.displayName(chat, userEmail)}</b>
                    </ul>
                  </React.Fragment>
                );
              })}
            </list>
          </div>
        </React.Fragment>
      );
    else
      return (
        <React.Fragment>
          <button
            variant="contained"
            className="btn btn-dark"
            id="newChatButton"
            onClick={this.props.newChatHandler}
          >
            Create new chat
          </button>
        </React.Fragment>
      );
  }
}

export default Conversations;
